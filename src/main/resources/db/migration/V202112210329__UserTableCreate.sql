
CREATE TABLE `users`
             (
                          `id`              bigint(20) NOT NULL auto_increment,
                          `code`            varchar(255) NULL DEFAULT NULL,
                          `first_name`      varchar(255) NULL DEFAULT NULL,
                          `middle_name`     varchar(255) NULL DEFAULT NULL,
                          `last_name`       varchar(255) NULL DEFAULT NULL,
                          `print_name`      varchar(255) NULL DEFAULT NULL,
                          `email_id`        varchar(255) NULL DEFAULT NULL,
                          `mubile_number`   varchar(15) NULL DEFAULT NULL,
                          `password`        text NULL DEFAULT NULL,
                          `whether_deleted` bit(1) NULL DEFAULT NULL,
                          `last_update`     timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          `last_updated_by` bigint(20) NULL DEFAULT NULL,
                          
                          PRIMARY KEY (`id`),
                          CONSTRAINT `fk_last_updated_by`
                          FOREIGN KEY (`last_updated_by`) 
                          REFERENCES `users` (`id`)
             )
             engine=innodb DEFAULT charset=utf8;