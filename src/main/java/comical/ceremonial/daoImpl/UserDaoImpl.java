package comical.ceremonial.daoImpl;

import org.springframework.stereotype.Repository;

import comical.ceremonial.dao.UserDao;
import comical.ceremonial.entity.User;

/**
 * @author gajendra.verma
 *
 */
@Repository("UserDao")
public class UserDaoImpl extends GenericDaoImpl<User,Long>implements UserDao {


	public UserDaoImpl() {
		super(User.class);
	}
	
}
