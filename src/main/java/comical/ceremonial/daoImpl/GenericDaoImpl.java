package comical.ceremonial.daoImpl;

import comical.ceremonial.dao.GenericDao;
import comical.ceremonial.exception.CeremonialApplicationException;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author gajendra.verma
 */

@Repository
public abstract class GenericDaoImpl<E, PK extends java.io.Serializable> implements GenericDao<E, PK> {

	private final Class<? extends E> _entityClass;

	@Autowired
	SessionFactory sessionFactory;

	public GenericDaoImpl(final Class<? extends E> entityClass) {
		super();
		_entityClass = entityClass;
	}

	public GenericDaoImpl(SessionFactory sessionFactory, Class<? extends E> _entityClass) {
		super();
		this.sessionFactory = sessionFactory;
		this._entityClass = _entityClass;
	}

	protected Session getCurrentSession() throws CeremonialApplicationException {
		try {
			return sessionFactory.getCurrentSession();
		} catch (final Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public PK create(final E newInstance) throws CeremonialApplicationException {

		PK id = null;
		final Session session = getCurrentSession();
		try {
Transaction t = 			session.beginTransaction();
			id = (PK) session.save(newInstance);
			t.commit();
//			session.flush();
			
		} catch (final Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException();
		}
		return id;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<PK> saveAll(final List<E> newInstance) throws CeremonialApplicationException {

		List<PK> list = new ArrayList<>();
		final Session session = getCurrentSession();
		for (E local : newInstance) {
			PK id = null;

			try {
				id = (PK) session.save(local);
				list.add(id);
				getCurrentSession().flush();
			} catch (final Exception e) {
				e.printStackTrace();
				throw new CeremonialApplicationException();
			}
		}
		return list;
	}

	@Override
	public void createAll(final List<E> newInstances) throws CeremonialApplicationException {
		for (E newInstance : newInstances) {
			try {
				getCurrentSession().save(newInstance);
			} catch (final Exception e) {
				e.printStackTrace();
				throw new CeremonialApplicationException();
			}
		}
		try {
			getCurrentSession().flush();
		} catch (Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public E update(final E entity) throws CeremonialApplicationException {
		try {
			return (E) getCurrentSession().merge(entity);
		} catch (final Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException();
		}

	}

	@Override
	public void updateAll(final List<E> entities) throws CeremonialApplicationException {
		try {
			for (E e : entities) {
				getCurrentSession().merge(e);
			}
		} catch (final Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public E merge(final E entity) throws CeremonialApplicationException {

		try {
			return (E) getCurrentSession().merge(entity);
		} catch (final ConstraintViolationException e) {
			e.printStackTrace();
			throw new CeremonialApplicationException("Exception in Genric Merge");
		} catch (final Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException("Hibernate Exception in Merge");
		}

	}

	@Override
	public E get(final PK id) throws CeremonialApplicationException {
		try {
			return (E) getCurrentSession().get(_entityClass, id);
		} catch (final Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException();
		}
	}

	@Override
	@SuppressWarnings("deprecation")
	public Criteria getCriteria() throws CeremonialApplicationException {
		try {
			return getCurrentSession().createCriteria(_entityClass);
		} catch (final Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException();
		}
	}

	@Override
	@SuppressWarnings({ "deprecation", "rawtypes" })
	public Criteria getCriteria(final Class entityclass) throws CeremonialApplicationException {
		try {
			return getCurrentSession().createCriteria(entityclass);
		} catch (final Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public <V> List<V> executeCriteria(final Criteria criteria) throws CeremonialApplicationException {
		try {
			return criteria.list();
		} catch (final Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public <V extends Object> V getObject(final Class classs, final PK id) throws CeremonialApplicationException {
		try {
			return (V) getCurrentSession().get(classs, id);
		} catch (final Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException();
		}
	}

	@Override
	public List<E> findAll() throws CeremonialApplicationException {
		final Criteria criteria = getCriteria(_entityClass);
		final List<E> list = executeCriteria(criteria);
		return list;
	}

	@Override
	public void delete(E newInstance) throws CeremonialApplicationException {
		try {
			getCurrentSession().delete(newInstance);
		} catch (final Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public E findById(Long id) throws CeremonialApplicationException {
		final Criteria criteria = getCriteria(_entityClass);
		criteria.add(Restrictions.eq("id", id));
		return (E) criteria.uniqueResult();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<E> findAll(List<Long> ids) throws CeremonialApplicationException {
		if (ids != null && ids.size() > 0) {
			final Criteria criteria = getCriteria(_entityClass);
			criteria.add(Restrictions.in("id", ids));
			return criteria.list();
		}
		return new ArrayList<E>();
	}

	@Override
	public void deleteAll(List<Long> ids) throws CeremonialApplicationException {
		try {
			final Session session = getCurrentSession();
			for (final Long id : ids) {
				final Object object = session.load(_entityClass, id);
				session.delete(object);
			}
		} catch (final Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException();
		}
	}

	@Override
	public void saveOrUpdateEntity(Object newInstance) throws CeremonialApplicationException {
		final Session session = getCurrentSession();
		try {
			session.saveOrUpdate(newInstance);
		} catch (final Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException();
		}
	}

	@Override
	public E load(final PK id) throws CeremonialApplicationException {
		try {
			return (E) getCurrentSession().load(_entityClass, id);
		} catch (final Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException();
		}
	}

	@Override
	public void deleteAllInstance(List<E> entities) throws CeremonialApplicationException {
		try {
			final Session session = getCurrentSession();
			for (final E entity : entities) {
				session.delete(entity);
			}
		} catch (final Exception e) {
			e.printStackTrace();
			throw new CeremonialApplicationException();

		}
	}

}
