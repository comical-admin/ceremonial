package comical.ceremonial.wsEntity;

/**
 * @author gajendra.verma
 * 
 */
public class WSBaseEntity {

	protected Long id;

	public WSBaseEntity() {
	}

	public WSBaseEntity(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
