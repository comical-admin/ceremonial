package comical.ceremonial.wsEntity;

import java.util.Date;

/**
 * @author gajendra.verma
 * 
 */
public class WSUser {
	
	private String code;
	private String firstName;
	private String middleName;
	private String lastName;
	private String printName;
	private String emailId;
	private Boolean whetherDeleted;

	protected Long id;
	private Date lastUpdate;
	private WSNameValue lastUpdatedBy;

	private String password;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPrintName() {
		return printName;
	}

	public void setPrintName(String printName) {
		this.printName = printName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Boolean getWhetherDeleted() {
		return whetherDeleted;
	}

	public void setWhetherDeleted(Boolean whetherDeleted) {
		this.whetherDeleted = whetherDeleted;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public WSNameValue getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(WSNameValue lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
