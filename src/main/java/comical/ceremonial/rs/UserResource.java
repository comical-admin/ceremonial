package comical.ceremonial.rs;

import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import comical.ceremonial.entity.User;
import comical.ceremonial.exception.CeremonialApplicationException;
import comical.ceremonial.service.UserService;
import comical.ceremonial.utill.CeremonialRSConstants;
import comical.ceremonial.wsEntity.WSUser;

/**
 * @author gajendra.verma
 *
 */

@Path(CeremonialRSConstants.USER_RESOURCE)
public class UserResource {

	@Autowired
	UserService userService;

	@Autowired
	DozerBeanMapper mapper;

	@GET
	@Path(CeremonialRSConstants.CEREMONIALWELCOME)
	public String welcomeMessage() {
		System.out.println("Application Started successfully............!");
		return "Welcome To Ceremonail by Comical";
	}

	@POST
	@Path(CeremonialRSConstants.PATH_SAVE)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Long save(WSUser userModel) {

		User user = mapper.map(userModel, User.class);
		Long id = null;
		try {
			user.setLastUpdate(new Date());
			id = userService.create(user);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return id;
	}

	@PUT
	@Path(CeremonialRSConstants.PATH_UPDATE)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Long update(WSUser userModel) {
		return null;
	}

	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Long delete(WSUser userModel) {
		return null;
	}

	@GET
	@Path(CeremonialRSConstants.FIND_BY_ID + "/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public WSUser findByid(@PathParam("id") Long userId) {
		return null;

	}
}
