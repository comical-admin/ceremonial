package comical.ceremonial.rs;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import comical.ceremonial.utill.CeremonialRSConstants;
/**
 * @author gajendra.verma
 */
@Component
@ApplicationPath(CeremonialRSConstants.CEREMONIALJERSEYCONFIG)
public class JerseyAppConfig extends ResourceConfig {

	public JerseyAppConfig() {
		packages( "comical.ceremonial.rs");
	}

}
