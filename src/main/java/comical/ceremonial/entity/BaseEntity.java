package comical.ceremonial.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author gajendra.verma
 *
 */
@Entity
public class BaseEntity {

	protected Long id;
	private Date lastUpdate;
	private User lastUpdatedBy;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

//	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_update", nullable = false)
	public Date getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "last_updated_by")
	public User getLastUpdatedBy() {

		lastUpdatedBy = new User();
		lastUpdatedBy.setId(1L);
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(User lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

}
