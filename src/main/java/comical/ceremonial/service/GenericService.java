package comical.ceremonial.service;

import java.util.List;

import comical.ceremonial.exception.CeremonialApplicationException;

/**
 * @author gajendra.verma
 * 
 */

public interface GenericService<T> {

	public Long create(T entity) throws CeremonialApplicationException;

	T update(T entity) throws CeremonialApplicationException;

	void delete(T entity) throws CeremonialApplicationException;

	void delete(Long id) throws CeremonialApplicationException;

	void deleteAllInstance(List<T> entities) throws CeremonialApplicationException;

	public T findById(Long id) throws CeremonialApplicationException;

	public List<T> findAll() throws CeremonialApplicationException;

	void createAll(List<T> entities) throws CeremonialApplicationException;

	void updateAll(List<T> entities) throws CeremonialApplicationException;

	public List<T> findEntityInstanceByIds(List<Long> itemInstanceIds) throws CeremonialApplicationException;

}
