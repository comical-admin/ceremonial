package comical.ceremonial.utill;

/**
 * @author gajendra.verma
 *
 */
public class CeremonialRSConstants {

	/* User Resource */
	public static final String USER_RESOURCE = "/userResource";
	public static final String CEREMONIALWELCOME = "/welcome";

	/* Genric path */
	public static final String CEREMONIALJERSEYCONFIG = "/rest";
	public static final String FIND_BY_ID = "/findById";
	public static final String PATH_UPDATE = "/update";
	public static final String PATH_SAVE = "/create";

}
