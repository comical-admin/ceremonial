package comical.ceremonial;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author gajendra.verma
 *
 */

@SpringBootApplication
@ComponentScan(basePackages = { "comical.ceremonial.rs", "comical.ceremonial.serviceImpl",
		"comical.ceremonial.daoImpl" })
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public Mapper mapper() {
		return new DozerBeanMapper();
	}
}
