package comical.ceremonial.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import comical.ceremonial.dao.GenericDao;
import comical.ceremonial.dao.UserDao;
import comical.ceremonial.entity.User;
import comical.ceremonial.service.UserService;

/**
 * @author gajendra.verma
 *
 */
@Service
public class UserServiceImpl extends GenericServiceImpl<User> implements UserService {

	UserServiceImpl() {
		super();
	}

	@Autowired
	UserDao userDao;
	
	
	@Override
	public GenericDao<User, Long> getBaseDao() {
		return userDao;
	}

}
