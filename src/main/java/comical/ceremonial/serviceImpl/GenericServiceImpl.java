package comical.ceremonial.serviceImpl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import comical.ceremonial.dao.GenericDao;
import comical.ceremonial.exception.CeremonialApplicationException;
import comical.ceremonial.service.GenericService;

/**
 * @author gajendra.verma
 * 
 */

@Service
public abstract class GenericServiceImpl<T> implements GenericService<T> {

	public abstract GenericDao<T, Long> getBaseDao();
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Long create(T entity) throws CeremonialApplicationException {
		return (Long) getBaseDao().create(entity);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void createAll(List<T> entities) throws CeremonialApplicationException {
		for (T entity : entities) {
			getBaseDao().create(entity);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public T update(T entity) throws CeremonialApplicationException {
		return getBaseDao().merge(entity);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateAll(List<T> entities) throws CeremonialApplicationException {
		for (T entity : entities) {
			getBaseDao().merge(entity);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(T entity) throws CeremonialApplicationException {
		getBaseDao().delete(entity);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(Long id) throws CeremonialApplicationException {
		T entity = getBaseDao().load(id);
		getBaseDao().delete(entity);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deleteAllInstance(List<T> entities) throws CeremonialApplicationException {
		getBaseDao().deleteAllInstance(entities);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public T findById(Long id) throws CeremonialApplicationException {

		return getBaseDao().findById(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<T> findAll() throws CeremonialApplicationException {

		return getBaseDao().findAll();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<T> findEntityInstanceByIds(List<Long> itemInstanceIds) throws CeremonialApplicationException {
		return getBaseDao().findAll(itemInstanceIds);
	}

}
