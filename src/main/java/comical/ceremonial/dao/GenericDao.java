package comical.ceremonial.dao;

import java.util.List;

import org.hibernate.Criteria;

import comical.ceremonial.exception.CeremonialApplicationException;
/**
 * @author gajendra.verma
 */
@SuppressWarnings({ "rawtypes" })
public interface GenericDao<E, PK extends java.io.Serializable> {

	PK create(E newInstance) throws CeremonialApplicationException;

	List<PK> saveAll(List<E> newInstance) throws CeremonialApplicationException;

	void createAll(List<E> newInstances) throws CeremonialApplicationException;

	E update(E entity) throws CeremonialApplicationException;

	void updateAll(List<E> entities) throws CeremonialApplicationException;

	void saveOrUpdateEntity(Object newInstance) throws CeremonialApplicationException;

	void deleteAll(List<Long> ids) throws CeremonialApplicationException;

	List<E> findAll(List<Long> ids) throws CeremonialApplicationException;

	E findById(Long id) throws CeremonialApplicationException;

	void delete(E newInstance) throws CeremonialApplicationException;

	List<E> findAll() throws CeremonialApplicationException;

	<V> List<V> executeCriteria(final Criteria criteria) throws CeremonialApplicationException;

	E merge(E entity) throws CeremonialApplicationException;

	E get(PK id) throws CeremonialApplicationException;

	Criteria getCriteria() throws CeremonialApplicationException;

	Criteria getCriteria(Class entityclass) throws CeremonialApplicationException;

	<V extends Object> V getObject(final Class entityClass, final PK id) throws CeremonialApplicationException;

	E load(PK id) throws CeremonialApplicationException;

	void deleteAllInstance(List<E> entities) throws CeremonialApplicationException;

}
