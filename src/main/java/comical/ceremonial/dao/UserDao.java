package comical.ceremonial.dao;

import comical.ceremonial.entity.User;

/**
 * @author gajendra.verma
 *
 */
public interface UserDao extends GenericDao<User, Long> {

}
