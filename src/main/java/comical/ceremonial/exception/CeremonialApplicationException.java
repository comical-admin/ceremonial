package comical.ceremonial.exception;


/**
 * gajendra.verma
 */

public class CeremonialApplicationException extends Exception {

	private static final long serialVersionUID = 4265758284484258031L;

	private String message;
	protected long timestamp;
	private boolean logged;

	public CeremonialApplicationException() {
	}

	public CeremonialApplicationException(final String message) {
		this.setMessage(message);
		init();
	}

	public CeremonialApplicationException(final String message, final Throwable throwable) {
		super(throwable);
		this.setMessage(message);
		init();
	}
	
	public CeremonialApplicationException(Throwable error) {
		super(error);
	}

	private void init() {
		timestamp = System.nanoTime();
	}

	@Override
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public boolean isLogged() {
		return logged;
	}

	public void setLogged(boolean logged) {
		this.logged = logged;
	}

}
