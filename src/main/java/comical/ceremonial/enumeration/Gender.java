/**
 * 
 */
package comical.ceremonial.enumeration;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author gajendra.verma
 *
 */
public enum Gender {


	MALE("MALE","Male","02"),
	FEMALE("FEMALE","Female","01");

	private String codeName;
	private String codeValue;
	private String id;

	private Gender(String codeName, String codeValue, String id) {
		this.codeName = codeName;
		this.codeValue = codeValue;
		this.id = id;
	}
	// the identifierMethod
	public String toCodeName() {
		return codeName;
	}
	// the identifierMethod
	public String toId() {
		return id;
	}

	// the valueOfMethod
	public  static Gender fromId(String id) {
		switch(id) {
		case "02": return MALE;
		case "01": return FEMALE;
		default:
			return null;
		}
	}
	// the valueOfMethod
	public  static Gender fromCodeName(String codeName) {
		switch(codeName) {
		case "MALE": return MALE;
		case "FEMALE": return FEMALE;
		default:
			return null;
		}
	}
	@Override
	public String toString() {
		return super.toString();
	}

	@JsonValue
	public String toValue(){
		StringBuilder builder = new StringBuilder();
		builder.append(this.codeName);
		return builder.toString();
	}

	@JsonCreator
	public static Gender create (String value) {
		if(value == null) {
			return null;
		}
		for(Gender v : values()) {
			if(value.equals(v.codeName)) {
				return v;
			}
		}
		return null;
	}

	public String getCodeName() {
		return codeName;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getCodeValue() {
		return codeValue;
	}

	public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	}

	public static List<Gender> genders = new ArrayList<Gender>();
	static {
		genders.add(MALE);
		genders.add(FEMALE);
	}

	/**
	 * @return the collection of genders
	 */
	public static List<Gender> getGenders() {
		return genders;
	}

}